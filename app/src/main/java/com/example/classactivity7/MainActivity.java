package com.example.classactivity7;

import android.content.Intent;
import android.content.SharedPreferences;
import android.os.Bundle;
import android.view.View;
import android.widget.Button;
import android.widget.CheckBox;
import android.widget.EditText;

import androidx.appcompat.app.AppCompatActivity;

public class MainActivity extends AppCompatActivity {

    public static final String MY_PREFS_NAME = "MyPrefsFile";

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_main);

        checkPrefs();
        Button buttonSignIn = findViewById(R.id.buttonSignIn);
        buttonSignIn.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                onClickbuttonSignIn();
            }
        });
    }

    public void checkPrefs()
    {
        SharedPreferences prefs = getSharedPreferences(MY_PREFS_NAME, MODE_PRIVATE);
        EditText editTextName=findViewById(R.id.editTextName);
        EditText editTextPassword=findViewById(R.id.editTextPassword);
        CheckBox checkBoxRememberMe = findViewById(R.id.checkBoxRememberMe);
        if(prefs.contains("name"))
        {

//            checkBoxRememberMe.setChecked(true);
//            editTextName.setText(prefs.getString("name","name"));
//            editTextPassword.setText(prefs.getString("password","password"));
            Intent intent = new Intent(this,HomePage.class);
            startActivity(intent);

        }
        else
        {
            checkBoxRememberMe.setChecked(false);
        }
    }




    public void onClickbuttonSignIn() {
        SharedPreferences prefs = getSharedPreferences(MY_PREFS_NAME, MODE_PRIVATE);
        SharedPreferences.Editor editor=prefs.edit();
        EditText editTextName=findViewById(R.id.editTextName);
        String name=editTextName.getText().toString();
        EditText editTextPassword=findViewById(R.id.editTextPassword);
        String password=editTextPassword.getText().toString();
        CheckBox checkBoxRememberMe = findViewById(R.id.checkBoxRememberMe);
        boolean checked = checkBoxRememberMe.isChecked();

        if(name != null && password != null && checked == true)
        {
            editor.putString("name", name);
            editor.putString("password", password);
            editor.apply();
            Intent intent = new Intent(this,HomePage.class);
            startActivity(intent);

        }
        else
        {
            editor.clear();
            editor.apply();
            Intent intent = new Intent(this,HomePage.class);
            startActivity(intent);

        }
    }
}